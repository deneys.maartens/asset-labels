EG=l01.png
EG=f01.png
# EG=dmtx.png
# EG=s21.png

.PHONY: all
all:
	./mklabel.sh
	# ./dmtx.sh
	open $(EG) || display $(EG)

.PHONY: clean
clean:
	$(RM) [0-9][0-9].png _q[0-9][0-9].png _d[0-9][0-9].png hspace.png d[0-9][0-9].png q[0-9][0-9].png

.PHONY: clobber
clobber: clean
	$(RM) l[0-9][0-9].png f[0-9][0-9].png dmtx.png m[0-9][0-9].png
