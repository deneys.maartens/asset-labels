#! /usr/bin/env python

import argparse
import typing

from pystrich.datamatrix import DataMatrixEncoder


def main(output: typing.BinaryIO, data: str, cellsize: int) -> None:
    encoder = DataMatrixEncoder(data)
    encoder.save(output.name, cellsize=cellsize)


def cli():
    parser = argparse.ArgumentParser(
        prog='dmtx',
        description='Create DataMatrix 2D-barcode')
    parser.add_argument(
        "data",
        help="Data to encode")
    parser.add_argument(
        "-o",
        "--output",
        type=argparse.FileType('w'),
        help="PNG output file name")
    parser.add_argument(
        "-c",
        "--cellsize",
        type=int,
        default=3,
        help="Cell pixel size")
    args = parser.parse_args()
    main(args.output, args.data, args.cellsize)


if __name__ == "__main__":
    cli()
