#! /bin/bash
#
# create tiny DataMatrix labels composite label

i=0
while read REPLY
do
  (( i++ ))
  desc=$(cut -f1 <<<"$REPLY")
  manf=$(cut -f2 <<<"$REPLY")
  pnum=$(cut -f3 <<<"$REPLY")
  snum=$(cut -f4 <<<"$REPLY")
  echo $i, $desc . $manf . PN: $pnum . SN: $snum
  no=$(printf %02d $i)
  segno "$manf;$pnum;;$snum;$desc" -o _q$no.png
  pdm run ./dmtx.py -o _d$no.png "$manf;$pnum;;$snum;$desc"
done < <(grep Alveo SPC)

magick \
  -size 128x210 \
  canvas:white \
  \( \
    _d01.png \
    -monochrome \
  \) -geometry +5+5 -composite \
  \( \
    _d02.png \
    -monochrome \
  \) -geometry +5+70 -composite \
  \( \
    _d03.png \
    -monochrome \
  \) -geometry +5+135 -composite \
  \( \
    _d04.png \
    -monochrome \
  \) -geometry +70+5 -composite \
  \( \
    _d05.png \
    -monochrome \
  \) -geometry +70+70 -composite \
  \( \
    _d06.png \
    -monochrome \
  \) -geometry +70+135 -composite \
  -negate \
  PNG:dmtx.png

# -fin-
