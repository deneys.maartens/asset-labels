#! /bin/bash
#
# create Data Matrix flag label
#
# officially the fields in QR code are:
#   supplier-code;part-number;version;serial-number
# we extend this to add the device-name:
#   supplier-code;part-number;version;serial-number;device-name
# and because there is no version number for COTS hardware, that field is left empty:
#   supplier-code;part-number;;serial-number;device-name

set -e

TTF_R=Atkinson-Hyperlegible-Regular-102.otf

S=3  # scale
H=112

convert \
  -size $(( $S * 15 ))x1 \
  canvas:white \
  PNG:hspace.png

i=0
while read REPLY
do
  i=$((i+1))
  desc=$(cut -f1 <<<"$REPLY")
  manf=$(cut -f2 <<<"$REPLY")
  pnum=$(cut -f3 <<<"$REPLY")
  snum=$(cut -f4 <<<"$REPLY")
  echo $i, $desc . $manf . PN: $pnum . SN: $snum
  no=$(printf %02d $i)
  [ $no = 21 ] || continue
  pdm run ./dmtx.py -c2 "$manf;$pnum;;$snum;$desc" -o m$no.png
  echo "$manf;$pnum;;$snum;$desc"
  convert \
    -size $(( $S * 3 ))x$H \
    canvas:white \
    \( \
      -size $(( $H + $S * 0 ))x$H \
      canvas:white \
      \( \
        m$no.png \
        -trim \
      \) -geometry +$(( $S * 2 ))+$(( $S * 2 + 2 )) -composite \
    \) +append \
    \( \
      \( \
        -size 1x$(( $S * 3 + 1 )) \
        canvas:white \
      \) \
      \( \
        -size $(( $S * 200 ))x$H \
        canvas:white \
        -font $TTF_R \
        -pointsize $(( $S * 9 )) \
        -annotate +0+$(( $S * 10 )) "$desc" \
        -annotate +0+$(( $S * 18 )) "$manf" \
        -annotate +0+$(( $S * 26 )) "PN: $pnum" \
        -annotate +0+$(( $S * 34 )) "SN: $snum" \
        -trim \
      \) -append \
    \) +append \
    \( \
      -size $(( $S * 10 ))x1 \
      canvas:white \
    \) +append \
    -monochrome \
    -negate \
    PNG:l$no.png

  convert \
    l$no.png \
    -negate \
    hspace.png \
    +append \
    \( \
      -size 1x$H \
      canvas:white \
      -stroke black \
      -fill none \
      -draw "stroke-dasharray 1 20 path 'M 0,0 L 0,"$H"'" \
    \) +append \
    hspace.png \
    +append \
    \( \
      l$no.png \
      -negate \
      -rotate 180 \
    \) +append \
    -negate \
    PNG:f$no.png

  convert \
    -size 1x$H \
    canvas:white \
    m$no.png \
    +append \
    -negate \
    PNG:s$no.png

done < <(sed 1d SPC)

# -fin-
