#! /bin/bash
#
# create QR code label
#
# officially the fields in QR code are:
#   supplier-code;part-number;version;serial-number
# we extend this to add the device-name:
#   supplier-code;part-number;version;serial-number;device-name
# and because there is no version number for COTS hardware, that field is left empty:
#   supplier-code;part-number;;serial-number;device-name

set -e

TTF_R=Atkinson-Hyperlegible-Regular-102.otf

S=3  # scale

case $S in
  2) H=112;;  # 18mm
  3) H=128;;  # 24mm
esac

magick \
  -size $(( $S * 45 ))x1 \
  canvas:white \
  PNG:hspace.png

i=0
while read REPLY
do
  i=$((i+1))
  desc=$(cut -f1 <<<"$REPLY")
  manf=$(cut -f2 <<<"$REPLY")
  pnum=$(cut -f3 <<<"$REPLY")
  snum=$(cut -f4 <<<"$REPLY")
  echo $i, $desc . $manf . PN: $pnum . SN: $snum
  no=$(printf %02d $i)
  segno --version 5 --scale $S "$manf;$pnum;;$snum;$desc" -o $no.png
  echo "$manf;$pnum;;$snum;$desc"
  magick \
    -size $(( $S * 3 ))x$H \
    canvas:white \
    \( \
      -size $(( 128 + $S * 4 ))x128 \
      canvas:white \
      \( \
        $no.png \
      \) -geometry +$(( $S * 0 ))-$(( $S * 1 )) -composite \
    \) +append \
    \( \
      \( \
        -size 1x$(( $S * 3 )) \
        canvas:white \
      \) \
      \( \
        -size $(( $S * 200 ))x$H \
        canvas:white \
        -font $TTF_R \
        -pointsize $(( $S * 11 )) \
        -annotate +$(( $S * 48 ))+$(( $S * 9 )) "$desc" \
        -annotate +$(( $S * 48 ))+$(( $S * 19 )) "$manf" \
        -annotate +$(( $S * 48 ))+$(( $S * 29 )) "PN: $pnum" \
        -annotate +$(( $S * 48 ))+$(( $S * 39 )) "SN: $snum" \
        -trim \
      \) -append \
    \) +append \
    \( \
      -size $(( $S * 10 ))x1 \
      canvas:white \
    \) +append \
    -monochrome \
    -negate \
    PNG:l$no.png

  magick \
    l$no.png \
    -negate \
    hspace.png \
    +append \
    \( \
      -size 1x$H \
      canvas:white \
      -stroke black \
      -fill none \
      -draw "stroke-dasharray 1 20 path 'M 0,0 L 0,"$H"'" \
    \) +append \
    hspace.png \
    +append \
    \( \
      l$no.png \
      -negate \
      -rotate 180 \
    \) +append \
    -negate \
    PNG:f$no.png

done < <(sed 1d SPC)

# -fin-
