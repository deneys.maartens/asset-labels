#! /bin/bash
#
# create QR code

i=0
while read REPLY
do
  (( i++ ))
  desc=$(cut -f1 <<<"$REPLY")
  manf=$(cut -f2 <<<"$REPLY")
  pnum=$(cut -f3 <<<"$REPLY")
  snum=$(cut -f4 <<<"$REPLY")
  echo $i, $desc . $manf . PN: $pnum . SN: $snum
  no=$(printf %02d $i)
  segno "$manf;$pnum;;$snum;$desc" -o _q$no.png
  convert \
    -size 128x50 \
    canvas:white \
    \( \
      _q$no.png \
    \) -geometry +5+5 -composite \
    -negate \
    PNG:q$no.png
done < <(grep Alveo SPC)

# -fin-
